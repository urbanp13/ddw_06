from math import sqrt
from numpy import genfromtxt
import numpy as np
import pandas as pd
import operator

class Content():
    def __init__(self, _id, _metricValue):
        self.id = _id
        self.metricValue = _metricValue

    def __str__(self):
        return str(self.id) + ":" + str(self.metricValue)

class User():
    def __init__(self, _id, _ratedMovies, _genresList):
        self.id = _id
        self.ratedMovies = _ratedMovies
        len = _genresList.__len__()
        self.gv = np.zeros((len))

    def __str__(self):
        ret = "id=" + str(self.id)
        ret = ret + "\n"
        for i in self.gv:
            ret = ret + str(i) + " "
        return ret

    def updateGenreVector(self, _pos):
        self.gv[pos] += 1.0

class Movie():
    def __init__(self, _id, _name, _genres):
        self.id = _id
        self.name = _name
        self.genres = _genres

    def __str__(self):
        ret = "id=" + self.id + ", name=" + self.name + ", genres="
        flag = True
        for genre in self.genres:
            if flag:
                ret = ret + genre
                flag = False
            else:
                ret = ret + "|" + genre
        '''ret = ret + "\n"
        for i in self.gv:
            ret = ret + str(i) + " "'''
        return ret


    def buildGenreVector(self, _genresList):
        len = _genresList.__len__()
        self.gv = np.zeros((len))
        for i in range(0, len):
            if genresList[i] in self.genres:
                self.gv[i] = 1




def loadData(file):
    data = genfromtxt(file, delimiter=',', dtype=(int, int, float, int))
    return data

def user_sim_cosine_sim(person1, person2):
    AprodB = 0.0
    A = 0.0
    B = 0.0

    for k,v in person1.items():
        if k in person2:
            a = person1[k]
            b = person2[k]
            AprodB = AprodB + a * b
            A = A + a*a
            B = B + b*b
    if (sqrt(A) * sqrt(B) == 0):
        return -999
    return AprodB / (sqrt(A) * sqrt(B))

def user_sim_pearson_corr(person1, person2):
    A = 0.0
    B1 = 0.0
    B2 = 0.0
    sum1 = 0.0
    sum2 = 0.0

    for k, v in person1.items():
        sum1 = sum1 + v
    for k, v in person2.items():
        sum2 = sum2 + v

    mean1 = sum1/person1.__len__()
    mean2 = sum2/person2.__len__()

    for k, v in person1.items():
        if k in person2:
            a = person1[k]
            b = person2[k]
            A = A + (a-mean1) * (b-mean2)
            B1 = B1 + (a-mean1) * (a-mean1)
            B2 = B2 + (b-mean2) * (b-mean2)
    if (sqrt(B1) * sqrt(B2) == 0):
        return -999
    return A / (sqrt(B1) * sqrt(B2))

def most_similar_users(person, toFind, metricVector, numOfUsers):
    similar = []
    for i in range(1, numOfUsers):
        similar.append(Content(i, metricVector[i]))
        similar.sort(key=lambda x: x.metricValue, reverse=True)

    ret = []
    count = 0
    i = 0
    while (count < toFind):
        if similar[i].id != person:
            ret.append(similar[i])
            count = count + 1
        i = i + 1
    return ret

def user_recommendations(person, metricVector, usersDict):
    ret = set()
    retDict = dict()
    personMovs = usersDict[person]
    similar = most_similar_users(person, SIMILAR_NUM, metricVector, usersDict.__len__())
    for user in similar:
        favMovs = dict(sorted(usersDict[user.id].items(), key=operator.itemgetter(1), reverse=True))
        sum = 0.0
        for m in favMovs:
            sum = sum + favMovs[m]
        avg = sum/favMovs.__len__()
        count = 0
        for m in favMovs:
            if (m not in personMovs) and (favMovs[m] >= avg) and count < 10 and not retDict.has_key(m):
                ret.add(Content(m,favMovs[m]*user.metricValue))  #tady se muze jeden film pridat vickrat s ruznymi hodnocenimi
                retDict[m] = favMovs[m]*user.metricValue         #diky retDict uz nemuze
                count = count + 1
    return ret

def user_movie_cosine(user, movie):
    AprodB = 0.0
    A = 0.0
    B = 0.0

    for i in range(0, user.gv.__len__()):
        u = user.gv[i]
        m = movie.gv[i]
        AprodB = AprodB + u * m
        A = A + u * u
        B = B + m * m
    if (sqrt(A) * sqrt(B) == 0):
        res = -999
        return Content(movie.id, res)

    #print AprodB / (sqrt(A) * sqrt(B))
    res = AprodB / (sqrt(A) * sqrt(B))
    return Content(movie.id, res)


def user_movie_pearson(user, movie):
    A = 0.0
    B1 = 0.0
    B2 = 0.0
    sum1 = 0.0
    sum2 = 0.0

    for i in range(0, user.gv.__len__()):
        sum1 = sum1 + user.gv[i]
        sum2 = sum2 + movie.gv[i]

    mean1 = sum1 / user.gv.__len__()
    mean2 = sum2 / movie.gv.__len__()

    for i in range(0, user.gv.__len__()):
        u = user.gv[i]
        m = movie.gv[i]
        A = A + (u - mean1) * (m - mean2)
        B1 = B1 + (u - mean1) * (u - mean1)
        B2 = B2 + (m - mean2) * (m - mean2)
    res = A / (sqrt(B1) * sqrt(B2))
    return Content(movie.id, res)




def getTopRecommended(user, simVector, n):
    alreadySeen = user.ratedMovies
    res = []
    count = 0
    i = 0
    while(count < n and i < simVector.__len__()):
        if(simVector[i].id not in alreadySeen):
            count = count + 1
            res.append(simVector[i])
        i = i + 1
    return res



SELECTED_USER = 247
CF_WEIGHT = 0.5
CB_WEIGHT = 1.0-CF_WEIGHT
SIMILAR_NUM = 5
CONTENT_BASED_NUM = 50
MIN_RATING = 2.5
genresSet = set()
movies = []
moviesDict = dict()

df = pd.read_csv('movies.csv', quotechar='"')
for index, row in df.iterrows():
    row = [str(row[col]) for col in list(df)]
    genres = []
    for genre in row[2].split('|'):
        genres.append(genre)
        genresSet.add(genre)
    movies.append(Movie(row[0], row[1], genres))
    #moviesDict[row[0]] = Movie(row[0], row[1], genres)

genresList = sorted(list(genresSet), key=str.lower)
numOfGenres = genresList.__len__()
print 'numOfGenres', numOfGenres, 'genresList', genresList
numOfMovies = movies.__len__()
print 'numOfMovies', numOfMovies

genresDict = dict()
for i in range(0, numOfGenres):
    genresDict[genresList[i]] = i

#print genresDict.items()


for movie in movies:
    movie.buildGenreVector(genresList)
    moviesDict[movie.id] = movie

trainData = loadData('training.csv')[1:]
testData = loadData('testing.csv')[1:]

numOfRatings = trainData.__len__()

numOfUsers = int(trainData[numOfRatings - 1][0])
print 'numOfRatings', numOfRatings
print 'numOfUsers', numOfUsers, '\n'


userProfiles = np.zeros((numOfUsers+1, numOfGenres), dtype='u1')


trainUsersDict = [dict() for x in range(numOfUsers + 1)]
testUsersDict = [dict() for x in range(numOfUsers + 1)]
selectDict = dict()

for row in trainData:
    userID = row[0]
    movieID = row[1]
    rating = row[2]
    trainUsersDict[userID][movieID] = rating
    if userID == SELECTED_USER:
        selectDict[movieID] = rating

for row in testData:
    userID = row[0]
    movieID = row[1]
    rating = row[2]
    testUsersDict[userID][movieID] = rating
    if userID == SELECTED_USER:
        selectDict[movieID] = rating

users = []
for i in range(1, numOfUsers+1):
    ratedMovies = []
    for item in trainUsersDict[i].items():
        ratedMovies.append(item)
    users.append(User(i, ratedMovies, genresList))

for user in users:
    for movie in user.ratedMovies:                              #filmy, ktere user hodnotil
        if movie[1] >= MIN_RATING:                              #pokud je hodnotil kladne - movie[1] je rating
            for genre in moviesDict[str(movie[0])].genres:      #movie[0] je id
                pos = genresDict[genre]
                user.updateGenreVector(pos)                     #update, pricita jednicku pro dany zanr
    user.gv = user.gv / np.amax(user.gv)                        #normalizace na interval 0-1

print "Selected user", users[SELECTED_USER-1], '\n\n'
'''
for user in users:
    print user
for movie in movies:
    print movie
'''

userMovieCosine = [None] * numOfMovies
userMoviePearson = [None] * numOfMovies

for i in range(0, numOfMovies):
    userMovieCosine[i] = user_movie_cosine(users[SELECTED_USER-1], movies[i])
    userMoviePearson[i] = user_movie_pearson(users[SELECTED_USER-1], movies[i])


userMovieCosineSorted = sorted(userMovieCosine, key=operator.attrgetter('metricValue'), reverse=True)
userMoviePearsonSorted = sorted(userMoviePearson, key=operator.attrgetter('metricValue'), reverse=True)

matchCBcosine = 0
recommendedCBcosine = getTopRecommended(users[SELECTED_USER-1], userMovieCosineSorted, CONTENT_BASED_NUM)
print "Top " + str(CONTENT_BASED_NUM) + " movies recommended for user " + str(SELECTED_USER) + " based on CosSim, content based:"
for x in recommendedCBcosine:
    print moviesDict[x.id], ", cosSim:", x.metricValue
    #print moviesDict[x.id].id, moviesDict[x.id].name, x.metricValue
    if int(x.id) in selectDict and selectDict[int(x.id)] >= MIN_RATING:
        matchCBcosine = matchCBcosine + 1
print matchCBcosine, "matches", '\n\n\n'

matchCBpear = 0
recommendedCBpear = getTopRecommended(users[SELECTED_USER-1], userMoviePearsonSorted, CONTENT_BASED_NUM)
print "Top " + str(CONTENT_BASED_NUM) + " movies recommended for user " + str(SELECTED_USER) + " based on PearCorr, content based:"
for x in recommendedCBpear:
    print moviesDict[x.id], ", corrCoef:", x.metricValue
    #print moviesDict[x.id].id, moviesDict[x.id].name, x.metricValue
    if int(x.id) in selectDict and selectDict[int(x.id)] >= MIN_RATING:
        matchCBpear = matchCBpear + 1
print matchCBpear, "matches", '\n\n\n'


cosineSim = np.zeros((numOfUsers+1))
pearsonCorr = np.zeros((numOfUsers+1))

for i in range(1, numOfUsers):
    cosineSim[i] = user_sim_cosine_sim(trainUsersDict[SELECTED_USER], trainUsersDict[i])
    pearsonCorr[i] = user_sim_pearson_corr(trainUsersDict[SELECTED_USER], trainUsersDict[i])

#print cosineSim
#print pearsonCorr

#volba, jestli chceme pouzit trenovaci nebo testovaci data
usersDict = trainUsersDict
#usersDict = testUsersDict


matchCFcosine = 0
recommendedCFcosine = user_recommendations(SELECTED_USER, cosineSim, usersDict)
print "Top " + str(recommendedCFcosine.__len__()) + " movies recommended for user " + str(SELECTED_USER) + " based on CosSim, CF:"
for x in recommendedCFcosine:
    print moviesDict[str(x.id)], ", similar user rating:", x.metricValue
    #print moviesDict[str(x.id)].id, moviesDict[str(x.id)].name, x.metricValue
    if x.id in selectDict and selectDict[x.id] >= MIN_RATING:
        matchCFcosine = matchCFcosine + 1
print matchCFcosine, "matches", '\n\n\n'

matchCFpear = 0
recommendedCFpear = user_recommendations(SELECTED_USER, pearsonCorr, usersDict)
print "Top " + str(recommendedCFpear.__len__()) + " movies recommended for user " + str(SELECTED_USER) + " based on PearCorr, CF:"
for x in recommendedCFpear:
    print moviesDict[str(x.id)], ", similar user rating:", x.metricValue
    #print moviesDict[str(x.id)].id, moviesDict[str(x.id)].name, x.metricValue
    if x.id in selectDict and selectDict[x.id] >= MIN_RATING:
        matchCFpear = matchCFpear + 1
print matchCFpear, "matches", '\n\n\n'

#pro normalizaci
maxCBcosine = (max(userMovieCosine, key=operator.attrgetter('metricValue'))).metricValue
maxCBpear = (max(userMoviePearson, key=operator.attrgetter('metricValue'))).metricValue
maxCFcosine = (max(recommendedCFcosine, key=operator.attrgetter('metricValue'))).metricValue
maxCFpear = (max(recommendedCFpear, key=operator.attrgetter('metricValue'))).metricValue

hybridScoresCosine = dict()
hybridScoresPear = dict()


for x in recommendedCFcosine:
    hybridScoresCosine[x.id] = CF_WEIGHT*x.metricValue/maxCFcosine + CB_WEIGHT*user_movie_cosine(users[SELECTED_USER-1], moviesDict[str(x.id)]).metricValue/maxCBcosine

for x in recommendedCFpear:
    hybridScoresPear[x.id] = CF_WEIGHT*x.metricValue/maxCFpear + CB_WEIGHT*user_movie_pearson(users[SELECTED_USER-1], moviesDict[str(x.id)]).metricValue/maxCBpear

hybridScoresCosine = sorted(hybridScoresCosine.items(), key=operator.itemgetter(1), reverse=True)
hybridScoresPear = sorted(hybridScoresPear.items(), key=operator.itemgetter(1), reverse=True)


print "Hybrid recommender system with following ratios (weights) - CB: " + str(CB_WEIGHT) + " and CF: " + str(CF_WEIGHT) + "."
matchHcosine = 0
print "Top " + str(hybridScoresCosine.__len__()) + " movies recommended for user " + str(SELECTED_USER) + " by hybrid RS, CosSim:"
for k, v in hybridScoresCosine:
    print moviesDict[str(k)], ", hybrid score:", v
    if k in selectDict and selectDict[k] >= MIN_RATING:
        matchHcosine = matchHcosine + 1
print matchHcosine, "matches", '\n\n\n'

matchHpear = 0
print "Top " + str(hybridScoresPear.__len__()) + " movies recommended for user " + str(SELECTED_USER) + " by hybrid RS, PearCorr:"
for k, v in hybridScoresPear:
    print moviesDict[str(k)], ", hybrid score:", v
    if k in selectDict and selectDict[k] >= MIN_RATING:
        matchHpear = matchHpear + 1
print matchHpear, "matches", '\n\n\n'




